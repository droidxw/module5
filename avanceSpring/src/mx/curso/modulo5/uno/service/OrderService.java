package mx.curso.modulo5.uno.service;

import mx.curso.modulo5.uno.repository.ArticuloRepository;
import mx.curso.modulo5.uno.repository.ArticuloRepositoryImpl;
import mx.curso.modulo5.uno.repository.OrderRepository;
import mx.curso.modulo5.uno.repository.dto.ArticuloDTO;
import mx.curso.modulo5.uno.service.dto.Articulo;

public class OrderService {

	private ArticuloRepository articuloRepository;
	
	private ArticuloRepository orderRepository;
	
//	public OrderService( ArticuloRepository articuloRepository) {
//		this.articuloRepository = articuloRepository;
//			
//}
	
	public OrderService( ArticuloRepository articuloRepository, ArticuloRepository orderRepository) {
		this.articuloRepository =  articuloRepository;
		this.orderRepository =  orderRepository;
		
			
}

public Articulo getArticuloPorId(Integer articuloId) {

	ArticuloDTO dto =articuloRepository.getPorArticuloId(articuloId);
	if(dto!=null) {
		return new Articulo( dto.getArticuloId(), dto.getTipo(), dto.getFabricante(), dto.getPrecio(), dto.getExistencias());
			}
	
	return null;
	
		
	}

public Articulo getOrderId(Integer orderId) {
	ArticuloDTO dto =orderRepository.getPorArticuloId(orderId);
	
	while(dto!=null) {
		return new Articulo(dto.getOrderId(), dto.getArticuloId());
			}
	return null;
	}


//public Articulo getTotalOrder(Integer orderId) {
//	ArticuloDTO dto =orderRepository.getPorArticuloId(orderId);
//	
//	while(dto!=null) {
//		return new Articulo(dto.getOrderId(), dto.getArticuloId(),dto.getPrecio());
//			}
//	return null;
//	}

}
