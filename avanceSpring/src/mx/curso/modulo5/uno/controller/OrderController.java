package mx.curso.modulo5.uno.controller;

import mx.curso.modulo5.uno.repository.dto.ArticuloDTO;
import mx.curso.modulo5.uno.service.OrderService;
import mx.curso.modulo5.uno.service.dto.Articulo;


public class OrderController {

	private OrderService orderService;

	
public OrderController(OrderService orderService) {

this.orderService=orderService;
}



	
	public void consultarPrecio(Integer articuloId) {

		Articulo articulo=orderService.getArticuloPorId(articuloId);
		if(articulo==null) {
			System.out.printf("El precio para el articulo con Id %d no existe \n", articuloId);
		}else {
			
			System.out.printf("Informacion del producto con Id "+articulo.getArticuloId()+": "+articulo.getTipo()+" "+ articulo.getFabricante()+" "+articulo.getPrecio()+"\n" );
		}	
		
	}
	
	public void montoTotal(Integer articuloId, Integer cantidad) {

		Articulo articulo=orderService.getArticuloPorId(articuloId);
		if(articulo==null) {
			System.out.printf("El articulo con Id %d no existe \n", articuloId);
		}else {
			
			System.out.printf("Monto total "+": "+articulo.getPrecio()+"x"+cantidad+"= "+articulo.getPrecio()*cantidad+" "+"Existencias "+articulo.getExistencias());
		}	
		
	}
	
	public void consultarOrden(Integer orderId) {

		Articulo articulo=orderService.getOrderId(orderId);
		if(articulo==null) {
			System.out.printf("La orden para el Id %d no existe \n", orderId);
		}else {
			
//			System.out.printf("Los articulos para el Id %d son %d \n",articulo.getOrderId(),articulo.getArticuloId(),articulo.getPrecio());
//			System.out.printf("El articulo de la orden %d es el numero %d \n", articulo.getArticuloId(),articulo.getPrecio());		
			System.out.printf("Informacion del producto con Id "+articulo.getArticuloId()+": "+articulo.getTipo()+" "+ articulo.getFabricante()+" "+articulo.getPrecio()+"\n" );
		}	
		
	}
	

}
