package mx.curso.modulo5.uno.repository;

import java.util.ArrayList;
import java.util.List;
import mx.curso.modulo5.uno.repository.dto.ArticuloDTO;

public class ArticuloRepositoryImpl implements ArticuloRepository {//articuloRepositoryImpl

	private List<ArticuloDTO> listaArticulos;
	
	
	public ArticuloRepositoryImpl() {
		listaArticulos=new ArrayList<>();
		//id, tipo, fabricante, precio existencias
		listaArticulos.add(new ArticuloDTO(1,"Server","Dell",7500, 50));
		listaArticulos.add(new ArticuloDTO(2,"Laptop","Lenovo",3500, 30));
		listaArticulos.add(new ArticuloDTO(35,"Desktop","IBM",3500, 2));
				
	}
	
	public ArticuloDTO getPorArticuloId(Integer articuloId) {
		for(ArticuloDTO articulo: listaArticulos) {
			
				if(articulo.getArticuloId().equals(articuloId)) {
					return articulo;
					
				}
			}
			return null;
		}
		
		
	}
	
	

