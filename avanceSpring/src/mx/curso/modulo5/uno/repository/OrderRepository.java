package mx.curso.modulo5.uno.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import mx.curso.modulo5.uno.repository.dto.ArticuloDTO;

public class OrderRepository implements ArticuloRepository  {
	


	private List<ArticuloDTO> listaArticulos;
//	private List listaC=new ArrayList<>(); 
	
	public OrderRepository() {
		listaArticulos=new ArrayList<>();
		listaArticulos.add(new ArticuloDTO(1,12));
		listaArticulos.add(new ArticuloDTO(1,24));
		listaArticulos.add(new ArticuloDTO(2,15));
		listaArticulos.add(new ArticuloDTO(3,16));
		listaArticulos.add(new ArticuloDTO(3,19));
		
				
	}
	
	public ArticuloDTO getPorArticuloId(Integer orderId) {
		for(ArticuloDTO articulo: listaArticulos) {
			
				if(articulo.getArticuloId().equals(orderId)) {
					return articulo;
					
				}
			}
	
			return null;
		}
		
	
	
	@Override
	public String toString() {
		System.out.println(listaArticulos);
		return "OrderRepository [listaArticulos=" + listaArticulos + "]";

	}

}
