package mx.curso.modulo5.uno.repository.dto;

public class ArticuloDTO {
	
	private Integer articuloId;
	
	private Integer orderId;	

	private Integer precio;	
	
	private String tipo;
	
	private String fabricante;
	
	private Integer existencias;

	public ArticuloDTO(Integer productoId, String tipo  ,String fabricante, Integer precio, Integer existencias) {
		super();
		this.articuloId = productoId;
		this.tipo = tipo;
	    this.fabricante=fabricante;
	    this.precio = precio;
	    this.existencias = existencias;
	}	
	
	
	public ArticuloDTO(Integer articuloId, Integer precio) {
		super();
		this.articuloId = articuloId;
		this.precio = precio;
	}

	
	public Integer getArticuloId() {
		return articuloId;
	}
	public void setArticuloId(Integer articuloId) {
		this.articuloId = articuloId;
	}
	public Integer getPrecio() {
		return precio;
	}
	public void setPrecio(Integer precio) {
		this.precio = precio;
	}
	
	public Integer getOrderId() {
		return orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	////

	public String getTipo() {
		return tipo;
	}
	
	
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	
	public String getFabricante() {
		return fabricante;
	}
	
	
	public void setFabricante(String fabricante) {
		this.fabricante = fabricante;
	}

	public Integer getExistencias() {
		return existencias;
	}


	public void setExistencias(Integer existencias) {
		this.existencias = existencias;
	}

}
