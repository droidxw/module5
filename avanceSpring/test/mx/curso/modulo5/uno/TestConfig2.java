package mx.curso.modulo5.uno;

import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.Resource;
//import org.springframework.jdbc.core.JdbcTemplate;

//import mx.curso.modulo5.uno.repository.ClimaRepository;

@Configuration
@ComponentScan(basePackages= {
		"mx.curso.modulo5.uno.repository",
})

@PropertySource("classpath:application.properties")
public class TestConfig2 {	

//	@Bean
//	@Qualifier("jdbcTemplate")
//	public JdbcTemplate mockjdbcTemplate() {
//		
//		return Mockito.mock(JdbcTemplate.class);
//	}	
//	
	
//	@Bean	
//	public Resource recurso (@Value ("${archivo.ruta1}") Resource recurso) {
//		
//		return recurso;
//	}	

}

