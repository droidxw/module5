//package mx.curso.modulo5.uno.service;
//
//import static org.junit.Assert.*;
//
//import org.junit.After;
//import org.junit.AfterClass;
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.BeforeClass;
//import org.junit.Ignore;
//import org.junit.Test;
//import org.mockito.Mockito;
//
//import mx.curso.modulo5.uno.repository.ClimaLista;
//import mx.curso.modulo5.uno.repository.ClimaRepository;
//import mx.curso.modulo5.uno.repository.dto.ClimaDTO;
//import mx.curso.modulo5.uno.service.dto.Clima;
//
//public class ClimaServiceTest {
//	
////	private ClimaService climaService;
////	
////	@Before
////	public void setUp() throws Exception{
////		ClimaLista climaLista=new ClimaLista();
////		climaLista.afterPropertiesSet();
////		
////		climaService=new ClimaService(climaLista);		
////	}
//	
//	private static ClimaService climaService;
//	private static ClimaRepository mockClimaRepository;
//	
//	
////	@BeforeClass
////	public static void setUpBeforeClass() throws Exception{
////		ClimaLista climaLista=new ClimaLista();
////		climaLista.afterPropertiesSet();
////		
////		climaService=new ClimaService(climaLista);		
////		climaService.inicializar();
////	}
//	
////	@BeforeClass
////	public static void setUpBeforeClass() throws Exception{
////		ClimaRepository stubClimaRepository=new ClimaRepository() {
////		
////			public ClimaDTO getPorCiudadId(Integer ciudadId) {
////		if (ciudadId==15)	{
////			return new  ClimaDTO(15,70);
////		}else {
////			return null;
////		}
////	}
////		public Integer getTotalClimas() {
////			return 100;
////			}
////		};
////		climaService=new ClimaService(stubClimaRepository);				
////	}
////	
//	
//	
//	@BeforeClass
//	public static void setUpBeforeClass() throws Exception{
//		mockClimaRepository=Mockito.mock(ClimaRepository.class);		
//		climaService=new ClimaService(mockClimaRepository);		
//
//	}
//	
////	@Test
////	public void testGetClimaPorCiudadIdExitoso() {
//////		fail("Not yet implemented");
////		Clima clima=climaService.getClimaPorCiudadId(1);
////		assertEquals(Integer.valueOf(1),clima.getCiudadId());
////		assertEquals(Integer.valueOf(25),clima.getTemperatura());
////		Assert.assertNotNull(clima);
////
////	}
//	
//	
//	@Test
//	public void testGetClimaPorCiudadIdExitoso() {
//		Mockito.when(mockClimaRepository.getPorCiudadId(15)).thenReturn(new ClimaDTO(15,70));
//		Clima clima=climaService.getClimaPorCiudadId(15);
//		assertEquals(Integer.valueOf(15),clima.getCiudadId());
//		assertEquals(Integer.valueOf(70),clima.getTemperatura());
//	}
//	
////	@Ignore
////	@Test
////	public void testGetClimaPorCiudadIdExitosoNoEncontrado() {
//////		fail("Not yet implemented");
////		Clima clima=climaService.getClimaPorCiudadId(5);
////		
////		assertNull(clima);
////		
////	}
//	
//	
//	@Test
//	public void testGetClimaPorCiudadIdNoEncontrado() {
//		Mockito.when(mockClimaRepository.getPorCiudadId(2)).thenReturn(null);
//		Clima clima=climaService.getClimaPorCiudadId(2);
//		Assert.assertNull(clima);
//		Mockito.verify(mockClimaRepository,Mockito.times(1)).getPorCiudadId(Mockito.eq(2));
//		Mockito.verify(mockClimaRepository,Mockito.times(1)).getPorCiudadId(Mockito.anyInt());
//	}
//
////	@Test
////	public void testGetTotalClimas() {
//////		fail("Not yet implemented");
////		Integer totalClimas=climaService.getTotalClimas();
////		assertEquals(Integer.valueOf(3),totalClimas);
////	}
//	
//	
//	@Test
//	public void testGetTotalClimas() {
//		Mockito.when(mockClimaRepository.getTotalClimas()).thenReturn(100);
//		Integer totalClimas=climaService.getTotalClimas();
//		Assert.assertEquals(Integer.valueOf(100),totalClimas);
//	}
//	
//	
//	@After
//	public void tearDown(){
//		Mockito.clearInvocations(mockClimaRepository);
//		
//	}
//	
//	
//	
//	@AfterClass
//	public static void tearDownAfterClass(){
//		Mockito.clearInvocations(mockClimaRepository);
//		climaService.destruir();
//	}
//	
//	
//
//}
