package mx.curso.modulo5.uno.service;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import mx.curso.modulo5.uno.TestConfig;
import mx.curso.modulo5.uno.repository.ArticuloRepository;
//import mx.curso.modulo5.uno.repository.ClimaLista;
//import mx.curso.modulo5.uno.repository.ArticuloRepositoryImpl;
import mx.curso.modulo5.uno.repository.dto.ArticuloDTO;
import mx.curso.modulo5.uno.service.dto.Articulo;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=TestConfig.class)

public class ClimaServiceTest2 {
	

	@Autowired
	private  OrderService orderService;
	@Autowired
	private ArticuloRepository mockArticuloRepository;		
	
	@Test
	public void testGetClimaPorCiudadIdExitoso() {
		Mockito.when(mockArticuloRepository.getPorArticuloId(15)).thenReturn(new ArticuloDTO(15,70));
		Articulo articulo=orderService.getArticuloPorId(15);
		assertEquals(Integer.valueOf(1),articulo.getPrecio());
		assertEquals(Integer.valueOf(5),articulo.getPrecio());
	}

	
//	@Test
//	public void testGetClimaPorCiudadIdNoEncontrado() {
//		Mockito.when(mockClimaRepository.getPorCiudadId(2)).thenReturn(null);
//		Clima clima=climaService.getClimaPorCiudadId(2);
//		Assert.assertNull(clima);
//		Mockito.verify(mockClimaRepository,Mockito.times(1)).getPorCiudadId(Mockito.eq(2));
//		Mockito.verify(mockClimaRepository,Mockito.times(1)).getPorCiudadId(Mockito.anyInt());
//	}
//
//
//	@Test
//	public void testGetTotalClimas() {
//		Mockito.when(mockClimaRepository.getTotalClimas()).thenReturn(100);
//		Integer totalClimas=climaService.getTotalClimas();
//		Assert.assertEquals(Integer.valueOf(100),totalClimas);
//	}
//	
//	
//	@After
//	public void tearDown(){
//		Mockito.clearInvocations(mockClimaRepository);
//		
//	}

	

}
