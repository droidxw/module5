package mx.curso.modulo5.uno;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import mx.curso.modulo5.uno.controller.OrderController;
import mx.curso.modulo5.uno.repository.OrderRepository;
import mx.curso.modulo5.uno.repository.ArticuloRepository;
import mx.curso.modulo5.uno.repository.ArticuloRepositoryImpl;
import mx.curso.modulo5.uno.service.OrderService;

@Configuration
public class ApplicationConfig {
	
	@Bean
	public OrderController orderController(OrderService orderService){
		
		return new OrderController(orderService);
	}

		
	@Bean	
	public OrderService orderService(@Qualifier("articuloRepositoryImpl") ArticuloRepository articuloRepository,
									 @Qualifier("orderRepository") ArticuloRepository orderRepository){
		
		return new  OrderService(articuloRepository, orderRepository);
	}
	
	
//	@Bean
//	public OrderService orderService(@Qualifier("orderRepository") ArticuloRepository orderRepository){
//		
//		return new  OrderService(orderRepository);
//}
	
	
	@Bean
	public ArticuloRepository articuloRepositoryImpl(){
		
		return new ArticuloRepositoryImpl();
	}
	
	@Bean
	public ArticuloRepository orderRepository(){
		
		return new OrderRepository();
	}
	
	
	
	

}
