package mx.curso.modulo5.uno.service.dto;
//POJOS

//atributos privados y constructores 
//Getters y Setters 
public class Articulo {
	
	
	private Integer articuloId;
	
	private Integer orderId;	

	private Integer precio;	
	

	public Integer getArticuloId() {
		return articuloId;
	}

	public void setArticuloId(Integer articuloId) {
		this.articuloId = articuloId;
	}

	public Integer getPrecio() {
		return precio;
	}

	public void setPrecio(Integer precio) {
		this.precio = precio;
	}
	
	public Integer getOrderId() {
		return orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}


	
	public Articulo(Integer orderId,Integer articuloId,Integer precio) {
		super();
		this.articuloId = articuloId;
		this.precio = precio;
		this.orderId=orderId;
			
		
	}	
	
	
	public Articulo (Integer articuloId, Integer precio) {
		super();		
		this.articuloId = articuloId;
		this.precio = precio;		
	}	

}
