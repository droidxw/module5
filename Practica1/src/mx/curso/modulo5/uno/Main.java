package mx.curso.modulo5.uno;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import mx.curso.modulo5.uno.controller.OrderController;
////Este proyecto no requiere anotacion de ningun tipo en sus clases
//// ya que  los componenetes se definen en la clase AplicationConfig
public class Main {

	public static void main(String args[]) {
		
		AnnotationConfigApplicationContext context=new  AnnotationConfigApplicationContext();
		context.register(ApplicationConfig.class);
		context.refresh();				

		//instancia del bean declarada en la clase de configuracion
		OrderController orderController =(OrderController) context.getBean("orderController");
		orderController.consultarPrecio(1);
		orderController.consultarPrecio(10);
		
		orderController.consultarOrden(3);	
		orderController.consultarOrden(2);
		
//		orderController.consultarOrden(2);
	
		
		
	}
	
}
