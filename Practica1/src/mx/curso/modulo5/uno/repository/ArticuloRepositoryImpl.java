package mx.curso.modulo5.uno.repository;

import java.util.ArrayList;
import java.util.List;
import mx.curso.modulo5.uno.repository.dto.ArticuloDTO;

public class ArticuloRepositoryImpl implements ArticuloRepository {//articuloRepositoryImpl

	private List<ArticuloDTO> listaArticulos;
	
	
	public ArticuloRepositoryImpl() {
		listaArticulos=new ArrayList<>();
		listaArticulos.add(new ArticuloDTO(1,25));
		listaArticulos.add(new ArticuloDTO(2,17));
		listaArticulos.add(new ArticuloDTO(3,33));
				
	}
	
	public ArticuloDTO getPorArticuloId(Integer articuloId) {
		for(ArticuloDTO articulo: listaArticulos) {
			
				if(articulo.getArticuloId().equals(articuloId)) {
					return articulo;
					
				}
			}
			return null;
		}
		
		
	}
	
	

