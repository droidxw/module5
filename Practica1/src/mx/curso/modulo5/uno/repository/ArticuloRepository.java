package mx.curso.modulo5.uno.repository;

import mx.curso.modulo5.uno.repository.dto.ArticuloDTO;

public interface ArticuloRepository {
	public ArticuloDTO getPorArticuloId(Integer articuloId);

}
