package mx.curso.modulo5.uno;

import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.GenericApplicationContext;

import mx.curso.modulo5.uno.controller.ClimaController;
////Este proyecto no requiere anotacion de ningun tipo en sus clases
//// ya que  los componenetes se definen en la clase AplicationConfig
public class Main {

	public static void main(String args[]) {
		
		AnnotationConfigApplicationContext context=new  AnnotationConfigApplicationContext();
		context.register(ApplicationConfig.class);
		context.refresh();				

		ClimaController climaController =(ClimaController) context.getBean("climaController");
		climaController.consultarTemperatura(1);
		climaController.consultarTemperatura(10);
		
		
	}
	
}
