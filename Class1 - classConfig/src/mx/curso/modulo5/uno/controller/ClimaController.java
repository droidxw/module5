package mx.curso.modulo5.uno.controller;

import mx.curso.modulo5.uno.service.ClimaService;
import mx.curso.modulo5.uno.service.dto.Clima;

public class ClimaController {

	private ClimaService climaService;

	
public ClimaController(ClimaService climaService) {

this.climaService=climaService;
}

	
	public void consultarTemperatura(Integer ciudadId) {

		Clima clima=climaService.getClimaPorCiudadId(ciudadId);
		if(clima==null) {
			System.out.printf("El clima para la ciudad %d no existe \n", ciudadId);
		}else {
			
			System.out.printf("La temperatura para la ciudad %d es %d \n", clima.getCiudadId(), clima.getTemperatura());
		}	
		
	}

}
