package mx.curso.modulo5.uno.service;

import mx.curso.modulo5.uno.repository.ClimaRepository;
import mx.curso.modulo5.uno.repository.dto.ClimaDTO;
import mx.curso.modulo5.uno.service.dto.Clima;

public class ClimaService {

	private ClimaRepository climaRepository;

	private ClimaRepository climaRepositoryImpl;	
	
	public ClimaService( ClimaRepository climaRepository, 
			ClimaRepository climaRepositoryImpl) {
			this.climaRepository = climaRepository;
			this.climaRepositoryImpl = climaRepositoryImpl;
}

public Clima getClimaPorCiudadId(Integer ciudadId) {

	ClimaDTO dto =climaRepository.getPorCiudadId(ciudadId);
	if(dto!=null) {
		return new Clima(dto.getCiudadId(), dto.getTemperatura());
			}
	
	return null;
	
		
	}

}
