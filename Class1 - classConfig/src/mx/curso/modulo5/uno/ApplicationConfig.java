package mx.curso.modulo5.uno;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import mx.curso.modulo5.uno.controller.ClimaController;
import mx.curso.modulo5.uno.repository.ClimaArchivo;
import mx.curso.modulo5.uno.repository.ClimaRepository;
import mx.curso.modulo5.uno.repository.ClimaRepositoryImpl;
import mx.curso.modulo5.uno.service.ClimaService;

@Configuration
public class ApplicationConfig {
	
	@Bean
	public ClimaController climaController(ClimaService climaService){
		
		return new ClimaController(climaService);
	}
	
	@Bean
	public ClimaService climaService(@Qualifier("climaRepository") ClimaRepository climaRepositoryImpl, 
									ClimaRepository climaRepository){
		
		return new  ClimaService(  climaRepository, climaRepositoryImpl);
	}
	
	
	@Bean
	public ClimaRepository climaRepository(){
		
		return new ClimaRepositoryImpl();
	}
	
	@Bean
	public ClimaRepository climaArchivo(){
		
		return new ClimaArchivo();
	}
	
	
	
	

}
