package mx.curso.modulo5.uno.service;

import mx.curso.modulo5.uno.repository.ClimaArchivo;
import mx.curso.modulo5.uno.repository.ClimaRepository;
import mx.curso.modulo5.uno.repository.dto.ClimaDTO;
import mx.curso.modulo5.uno.service.dto.Clima;
//Clase para logica de negocio
public class ClimaService {
//se implementa un patron singleton para obetener la misma instancia  cada vez 
//que se llama y eficientar recursos y no crear constantemente nuevos objetos
	

//	private ClimaRepository climaRepository;
	//cambio de dependencia a un archivo en vez del repo
	private ClimaArchivo climaArchivo;
	
public ClimaService() { 	
	
//		this.climaRepository = new ClimaRepository();
	//cambio de dependencia a un archivo en vez del repo
	// con el operador new hacemos un acoplamiento de la clase con la dependencia que utiliza
//	con spring se reduce los acoplamientos al crear los objetos por medio de una interfaz
		this.climaArchivo = new ClimaArchivo();

	}

public Clima getClimaPorCiudadId(Integer ciudadId) {
	// cuando se llama al metodo que consume memoria siempre que se llama al crear objetos (ineficiente)
	//	ClimaRepository climaRepository=new ClimaRepository();	
	
//	ClimaDTO dto =climaRepository.getPorCiudadId(ciudadId);
	//cambio de dependencia a un archivo en vez del repo
	ClimaDTO dto =climaArchivo.getPorCiudadId(ciudadId);
	
	if(dto!=null) {
		return new Clima(dto.getCiudadId(), dto.getTemperatura());
			}
	
	return null;	
		
	}

}
