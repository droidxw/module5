package mx.curso.modulo5.uno.service.dto;
//Clase para intercambiar informacion (objetos) entre las otras clases
//POJOS Plain Old Java Object

//atributos y constructores privados (aqui faltaria hacer privado el constructor)
//Getters y Setters 
public class Clima {
	
	private Integer ciudadId;
	private Integer temperatura;
	
	public Clima(Integer ciudadId, Integer temperatura) {
		this.ciudadId=ciudadId;
		this.temperatura=temperatura;
	}
	
	public Integer getCiudadId() {
		return ciudadId;
	}

	public void setCiudadId(Integer ciudadId) {
		this.ciudadId = ciudadId;
	}

	public Integer getTemperatura() {
		return temperatura;
	}

	public void setTemperatura(Integer temperatura) {
		this.temperatura = temperatura;
	}



}
