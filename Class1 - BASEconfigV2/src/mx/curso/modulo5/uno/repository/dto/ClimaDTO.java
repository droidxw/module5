package mx.curso.modulo5.uno.repository.dto;
//Clase para intercambiar objetos entre las otras clases
public class ClimaDTO {
	
	private Integer ciudadId;
	private Integer temperatura;
	
	public ClimaDTO(Integer ciudadId, Integer temperatura) {
		this.ciudadId=ciudadId;
		this.temperatura=temperatura;
	}
	
	public Integer getCiudadId() {
		return ciudadId;
	}
	public void setCiudadId(Integer ciudadId) {
		this.ciudadId = ciudadId;
	}
	public Integer getTemperatura() {
		return temperatura;
	}
	public void setTemperatura(Integer temperatura) {
		this.temperatura = temperatura;
	}
	
	

}
