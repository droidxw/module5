package mx.curso.modulo5.uno.controller;

import mx.curso.modulo5.uno.repository.ClimaRepository;
import mx.curso.modulo5.uno.service.ClimaService;
import mx.curso.modulo5.uno.service.dto.Clima;
//Esta clase se encargarķa de hacer el front: peticiones, procesar y 
//obtener  el resultado de lo solicitado

public class ClimaController {

	private ClimaService climaService;
	
	public ClimaController() {
		this.climaService=new ClimaService();

	}

	
	public void consultarTemperatura(Integer ciudadId) {
//		ClimaService climaService=new ClimaService();
		Clima clima=climaService.getClimaPorCiudadId(ciudadId);
		if(clima==null) {
			System.out.printf("El clima para la ciudad %d no existe \n", ciudadId);
		}else {
			
			System.out.printf("La temperatura para la ciudad %d es %d \n", clima.getCiudadId(), clima.getTemperatura());
		}	
		
	}

}
