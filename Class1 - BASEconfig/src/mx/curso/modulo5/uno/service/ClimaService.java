package mx.curso.modulo5.uno.service;

import mx.curso.modulo5.uno.repository.ClimaRepository;
import mx.curso.modulo5.uno.repository.dto.ClimaDTO;
import mx.curso.modulo5.uno.service.dto.Clima;
//Clase para logica de negocio
public class ClimaService {
//se implementa un patron singleton para obetener la misma instancia  cada vez 
//que se llama y eficientar recursos y no crear constantemente nuevos objetos
	private ClimaRepository climaRepository;
	
public ClimaService() { 		
		this.climaRepository = new ClimaRepository();

	}

public Clima getClimaPorCiudadId(Integer ciudadId) {
	// cuando se llama al metodo que consume memoria siempre que se llama al crear objetos (ineficiente)
//	ClimaRepository climaRepository=new ClimaRepository();
	ClimaDTO dto =climaRepository.getPorCiudadId(ciudadId);
	if(dto!=null) {
		return new Clima(dto.getCiudadId(), dto.getTemperatura());
			}
	
	return null;	
		
	}

}
