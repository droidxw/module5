package mx.curso.modulo5.uno;

import java.io.File;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.core.io.Resource;

import mx.curso.modulo5.uno.controller.ClimaController;
import mx.curso.modulo5.uno.repository.ClimaArchivo;
import mx.curso.modulo5.uno.repository.ClimaRepository;
import mx.curso.modulo5.uno.repository.ClimaRepositoryImpl;
import mx.curso.modulo5.uno.service.ClimaService;

@Configuration
@ComponentScan(basePackages = { 
		"mx.curso.modulo5.uno.controller", 
		"mx.curso.modulo5.uno.service"
		})

@PropertySource("classpath:application.properties")
public class ApplicationConfig {

	@Bean
	// definicion de scope en beans
	// este scope recomendado para objetos de estado statefull
	@Scope("prototype")
	public ClimaRepository climaRepository() {

		return new ClimaRepositoryImpl();
	}

	@Bean(destroyMethod = "destruir", initMethod = "inicializar")
	// definicion de scope en beans
	@Scope("prototype")
//	public ClimaRepository climaArchivo(@Value("${archivo.nombre}") String nombreArchivo) {
	public ClimaRepository climaArchivo(@Value("${archivo.nombre}") String nombreArchivo, @Value("classpath:clima.csv") Resource recurso) {
//		"file:C:\\Users\\clima.csv"{ C:\workspace\M5\Class1 - varRead\resources

//		return new ClimaArchivo(nombreArchivo);
		return new ClimaArchivo(nombreArchivo, recurso);
//		 en xml:
//		<bean id="something" class="myclass" scope="prototype" 
	}

}
