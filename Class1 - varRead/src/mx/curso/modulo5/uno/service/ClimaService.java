package mx.curso.modulo5.uno.service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import mx.curso.modulo5.uno.controller.ClimaController;
import mx.curso.modulo5.uno.repository.ClimaRepository;
import mx.curso.modulo5.uno.repository.dto.ClimaDTO;
import mx.curso.modulo5.uno.service.dto.Clima;

@Service
public class ClimaService {
	private static Log log=LogFactory.getLog(ClimaController.class);

	private ClimaRepository climaRepository;
	//notacion de variable para la carga de valores	
	@Value("${db.user}")
	private String username;
	@Value("${db.pwd}")
	private String password;
	
	public ClimaService(@Qualifier("climaArchivo") ClimaRepository climaRepository) {
			log.info("testClimaService//"+climaRepository);	
		
			this.climaRepository = climaRepository;			
			
}	

public Clima getClimaPorCiudadId(Integer ciudadId) {
	ClimaDTO dto =climaRepository.getPorCiudadId(ciudadId);
	if(dto!=null) {
		return new Clima(dto.getCiudadId(), dto.getTemperatura());
			}
	
	return null;
	
		
	}
//metodo que se llama despues de la inyeccion sin importar el scope
@PostConstruct
public void inicializar() {
	log.info("Inicializando ClienteService");
	log.info("ClimaRepository" + climaRepository);
	log.info("username:"+username);
	log.info("password:"+password);
}


//metodo que se llama antes de la destruccion del bean sin importar el scope
@PreDestroy
public void destruir() {
	log.info("Destruyendo ClienteService");
	
}

}
