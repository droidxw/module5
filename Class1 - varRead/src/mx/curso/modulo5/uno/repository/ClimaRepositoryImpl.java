package mx.curso.modulo5.uno.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;


import mx.curso.modulo5.uno.controller.ClimaController;
import mx.curso.modulo5.uno.repository.dto.ClimaDTO;

public class ClimaRepositoryImpl implements ClimaRepository,
//se inicializa y se destruye con estas dos interfaces en la clase
InitializingBean, DisposableBean {//climaRepositoryImpl
	private static Log log=LogFactory.getLog(ClimaController.class);

	
	private List<ClimaDTO> listaClimas;
	
	public ClimaRepositoryImpl() {				
	}
	
	@Override
	public ClimaDTO getPorCiudadId(Integer ciudadId) {
		for(ClimaDTO clima: listaClimas) {
			
				if(clima.getCiudadId().equals(ciudadId)) {
					return clima;
					
				}
			}
			return null;
		}

//metodo de la interfaz InitializingBean 
	@Override
	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub
		log.info("Inicializando ClimaRepositoryImpl");
		listaClimas=new ArrayList<>();
		listaClimas.add(new ClimaDTO(1,25));
		listaClimas.add(new ClimaDTO(2,17));
		listaClimas.add(new ClimaDTO(3,33));
		log.info("climaRepo"+listaClimas);
	}
	
	
	//metodo de la interfaz DisposableBean
	@Override
	public void destroy() throws Exception {
		// TODO Auto-generated method stub
		listaClimas.clear();
	}
		
		
	}
	
	

