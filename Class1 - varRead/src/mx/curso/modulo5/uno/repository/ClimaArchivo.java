package mx.curso.modulo5.uno.repository;

import java.io.File;

import java.io.IOException;
import java.util.Scanner;

import org.springframework.core.io.Resource;
import org.springframework.stereotype.Repository;

import mx.curso.modulo5.uno.repository.dto.ClimaDTO;


public class ClimaArchivo implements ClimaRepository  {

	private String nombreArchivo;
	private Resource recurso;
	
//	public ClimaArchivo(String nombreArchivo) {
	public ClimaArchivo(String nombreArchivo, Resource recurso) {
		this.nombreArchivo= nombreArchivo;
		this.recurso=recurso;
	}
	
	public ClimaDTO getPorCiudadId(Integer ciudadId) {
		try {
			//se deja de utilizar el método get archivo
//			File archivo=new File(getArchivo());
			File archivo= recurso.getFile();		
			Scanner sc=new Scanner (archivo);
			while(sc.hasNextLine()) {
				String linea=sc.nextLine();
				String []valores=linea.split(",");
				if(valores[0].equals(ciudadId.toString())) {
					return new ClimaDTO(valores[0],valores[1]);
				}	
			}

		}catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	//se deja de utilizar el método get archivo al utilizar *.getFile()
//	private String getArchivo() {
//		// obtiene el nombre del archivo y además remueve espacios en blanco reeemplazandolos (*.replaceAll)
//		return getClass().getClassLoader().getResource(nombreArchivo).getFile().replaceAll("%20", " ");
//	}

	//se inicializan los beans despues de la inyeccion 
	private void inicializar() {		
		System.out.println("Inicializando ClimaArchivo");		
	}
	
	//al terminar de usar la aplicacion (cierre del contexto de sprint) se llama este metodo
	//esto antes de que se destruya el bean de inicio
private void destruir() {		
		System.out.println("Destruyendo ClimaArchivo");		
	}
		

}
