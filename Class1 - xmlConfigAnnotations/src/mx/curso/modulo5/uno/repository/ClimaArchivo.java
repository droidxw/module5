package mx.curso.modulo5.uno.repository;

import java.util.ArrayList;
import java.util.List;

import mx.curso.modulo5.uno.repository.dto.ClimaDTO;

public class ClimaArchivo implements ClimaRepository  {
	
	
	private List<ClimaDTO> listaClimas;
	
	
	public ClimaArchivo() {
		listaClimas=new ArrayList<>();
		listaClimas.add(new ClimaDTO(1,12));
		listaClimas.add(new ClimaDTO(2,24));
		listaClimas.add(new ClimaDTO(3,16));
				
	}
	
	public ClimaDTO getPorCiudadId(Integer ciudadId) {
		for(ClimaDTO clima: listaClimas) {
			
				if(clima.getCiudadId().equals(ciudadId)) {
					return clima;
					
				}
			}
			return null;
		}
		

}
