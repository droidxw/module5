package mx.curso.modulo5.uno.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

//import mx.curso.modulo5.uno.repository.ClimaRepository;
import mx.curso.modulo5.uno.repository.ClimaArchivo;
import mx.curso.modulo5.uno.repository.ClimaRepository;
import mx.curso.modulo5.uno.repository.dto.ClimaDTO;
import mx.curso.modulo5.uno.service.dto.Clima;

public class ClimaService {
	@Autowired
//	private ClimaRepository climaRepository;
	private ClimaArchivo climaRepository;


public Clima getClimaPorCiudadId(Integer ciudadId) {

	ClimaDTO dto =climaRepository.getPorCiudadId(ciudadId);
	if(dto!=null) {
		return new Clima(dto.getCiudadId(), dto.getTemperatura());
			}
	
	return null;
	
		
	}

}
