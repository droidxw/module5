package mx.curso.modulo5.uno;

import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.context.support.GenericApplicationContext;

import mx.curso.modulo5.uno.controller.ClimaController;

public class Main {

	public static void main(String args[]) {
		//crea el contexto
		GenericApplicationContext context=new  GenericApplicationContext();
		//Lee configuracion desde archivo xml
		new XmlBeanDefinitionReader(context).loadBeanDefinitions("application-context.xml");
		context.refresh();
		
//		ClimaController climaController =new ClimaController();
		ClimaController climaController =(ClimaController) context.getBean("climaController");
		climaController.consultarTemperatura(1);
		climaController.consultarTemperatura(10);
		
		
	}
	
}
