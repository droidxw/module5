package mx.curso.modulo5.uno.controller;


import mx.curso.modulo5.uno.service.ClimaService;
import mx.curso.modulo5.uno.service.dto.Clima;

public class ClimaController {

	private ClimaService climaService;
	
	public ClimaController() {
		//instancia adicional inecesaria
		this.climaService=new ClimaService();
		
		//Aqui se muestra como se crean nuevos objetos (con el operador new), cada vez 
       //que se llma al constructor, este c�digo con Spring es inecesario
		System.out.println("Bean en el main 1:"+this.climaService);
		this.climaService=new ClimaService();
		System.out.println("Bean en el main 1:"+this.climaService);
	}

	
	public void consultarTemperatura(Integer ciudadId) {
		Clima clima=climaService.getClimaPorCiudadId(ciudadId);
		if(clima==null) {
			System.out.printf("El clima para la ciudad %d no existe \n", ciudadId);
		}else {
			
			System.out.printf("La temperatura para la ciudad %d es %d \n", clima.getCiudadId(), clima.getTemperatura());
		}	
		
	}
	
	public void setClimaService(ClimaService climaservice) {
		this.climaService=climaservice;
	}
}
