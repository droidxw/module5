package mx.curso.modulo5.uno.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import mx.curso.modulo5.uno.repository.dto.ClimaDTO;

//import com.sun.tools.javac.util.List;

	public class ClimaRepository  {//climaRepositoryImpl

	private List<ClimaDTO> listaClimas;
	
	
	public ClimaRepository() {
		listaClimas=new ArrayList<>();
		listaClimas.add(new ClimaDTO(1,25));
		listaClimas.add(new ClimaDTO(2,17));
		listaClimas.add(new ClimaDTO(3,33));
				
	}
	
	public ClimaDTO getPorCiudadId(Integer ciudadId) {
		for(ClimaDTO clima: listaClimas) {
			
				if(clima.getCiudadId().equals(ciudadId)) {
					return clima;
					
				}
			}
			return null;
		}
		
		
	}
	
	

