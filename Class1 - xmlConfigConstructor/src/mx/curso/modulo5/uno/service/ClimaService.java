package mx.curso.modulo5.uno.service;
import mx.curso.modulo5.uno.repository.ClimaArchivo;
import mx.curso.modulo5.uno.repository.ClimaRepository;
import mx.curso.modulo5.uno.repository.dto.ClimaDTO;
import mx.curso.modulo5.uno.service.dto.Clima;

public class ClimaService {
//aqui solo se cambio el nombre de la instancia pero sigue siendo del mismo tipo definido en la clase y en el bean
	//se debe de agregar una interfaz que implementen las clases que obtienen los datos, para usarlas insdistintamente 
//	como atributos dentro de esta clase 
	private ClimaArchivo climaRepository;
	
public ClimaService(ClimaArchivo climaArchivo) {		
		this.climaRepository = climaArchivo;
	
	}

public Clima getClimaPorCiudadId(Integer ciudadId) {
	ClimaDTO dto =climaRepository.getPorCiudadId(ciudadId);
	if(dto!=null) {
		return new Clima(dto.getCiudadId(), dto.getTemperatura());
			}	
	return null;		
	}

}
