package mx.curso.modulo5.uno.repository.dto;

public class ClimaDTO {
	
	private Integer ciudadId;
	private Integer temperatura;
	
	public ClimaDTO(Integer ciudadId, Integer temperatura) {
		this.ciudadId=ciudadId;
		this.temperatura=temperatura;
	}
	
	public ClimaDTO(String ciudadId, String temperatura) {
		// TODO Auto-generated constructor stub
		this.ciudadId=Integer.decode(ciudadId);
		this.temperatura=Integer.decode(temperatura);
	}

	public Integer getCiudadId() {
		return ciudadId;
	}
	public void setCiudadId(Integer ciudadId) {
		this.ciudadId = ciudadId;
	}
	public Integer getTemperatura() {
		return temperatura;
	}
	public void setTemperatura(Integer temperatura) {
		this.temperatura = temperatura;
	}
	
	

}
