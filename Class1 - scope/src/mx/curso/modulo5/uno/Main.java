package mx.curso.modulo5.uno;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;


import mx.curso.modulo5.uno.controller.ClimaController;

public class Main {
	private static Log log=LogFactory.getLog(ClimaController.class);

	public static void main(String args[]) {		
		AnnotationConfigApplicationContext context=new  AnnotationConfigApplicationContext();
		context.register(ApplicationConfig.class);
		context.refresh();
		log.info("Contexto cargado\n");

//		se crean primero todos los beans singleton (los prototype hasta que se invocan)
//		pero si existe una dependencia con un bean prototype primero crea ese bean 

		ClimaController climaController = (ClimaController) context.getBean("climaController");
		log.info("Bean climaController1: " + climaController);
		climaController.consultarTemperatura(1);
		climaController.consultarTemperatura(10);
		log.info("--------\n");

		ClimaController climaController2 = (ClimaController) context.getBean("climaController");
		log.info("Bean climaController2: " + climaController2);
		climaController.consultarTemperatura(1);
		climaController.consultarTemperatura(10);
		//se debe cerrar el contexto con esta instruccion 
//		para que se puedan invocar los metodos de destruccion de beans definidos 
		context.close();
		log.info("--------\n");

	}
		
		
		
		
	
	
	
	
}
