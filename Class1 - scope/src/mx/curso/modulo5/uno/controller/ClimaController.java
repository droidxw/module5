package mx.curso.modulo5.uno.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;

import mx.curso.modulo5.uno.repository.ClimaRepository;
import mx.curso.modulo5.uno.service.ClimaService;
import mx.curso.modulo5.uno.service.dto.Clima;

@Controller
//definicion de scope en clase
@Scope("prototype")
public class ClimaController {
	private static Log log = LogFactory.getLog(ClimaController.class);

	private ClimaService climaService;

	public ClimaController(ClimaService climaService) {

		this.climaService = climaService;
		log.info("ClimaService" + climaService);

	}

	public void consultarTemperatura(Integer ciudadId) {

		Clima clima = climaService.getClimaPorCiudadId(ciudadId);
		if (clima == null) {
			System.out.printf("El clima para la ciudad %d no existe \n", ciudadId);
		} else {

			System.out.printf("La temperatura para la ciudad %d es %d \n", clima.getCiudadId(), clima.getTemperatura());
		}

	}

}
