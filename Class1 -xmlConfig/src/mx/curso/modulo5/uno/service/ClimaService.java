package mx.curso.modulo5.uno.service;

import mx.curso.modulo5.uno.repository.ClimaRepository;
import mx.curso.modulo5.uno.repository.dto.ClimaDTO;
import mx.curso.modulo5.uno.service.dto.Clima;

public class ClimaService {

	private ClimaRepository climaRepository;

public ClimaService() {		
	//esta instanciacion se remplaza con spring mediante la configuracion en el xml
//		this.climaRepository = new ClimaRepository();
	//muestra el  objeto creado manualmente y como cambia en cada invocacion  
//		System.out.print("Objeto en ClimaService\n" + climaRepository);
//		System.out.print("\n");
//		this.climaRepository = new ClimaRepository();
//		System.out.print("Objeto2 en ClimaService\n" + climaRepository);
//		System.out.print("\n");

	}

public Clima getClimaPorCiudadId(Integer ciudadId) {
	

	ClimaDTO dto =climaRepository.getPorCiudadId(ciudadId);
	if(dto!=null) {
		return new Clima(dto.getCiudadId(), dto.getTemperatura());
			}
	
	return null;
	
		
	}
// se crea un setter del atributo, para que spring pueda inyectar el bean, debido a que 
//el atributo al ser privado es la unica forma de accesarlo y configurarlo 
		public void setClimaRepository(ClimaRepository climaRepository) {
			this.climaRepository=climaRepository;
		}
}
