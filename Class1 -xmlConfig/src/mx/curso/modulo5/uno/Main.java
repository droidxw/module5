package mx.curso.modulo5.uno;

import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.context.support.GenericApplicationContext;

import mx.curso.modulo5.uno.controller.ClimaController;

public class Main {

	public static void main(String args[]) {
//Carga Spring para usarlo

		//Crea el  contexto(almacenamiento interno de objetos mediante una referencia/nombre) de spring(1 de diversas formas, 
		//en este caso para standalone)
		GenericApplicationContext context=new  GenericApplicationContext();
		//obtiene configuraciones xml cargadas por archivos, con comas puedo agregar más configuraciones		
		new XmlBeanDefinitionReader(context).loadBeanDefinitions("application-context.xml");
		context.refresh();

		//forma previa para crear objetos
		//ClimaController climaController =new ClimaController();

		//aqui spring ya creo nuestros objetos, y con esta linea ACCEDEMOS al contexto de los objetos(Beans denominacion de spring) que 
		// crea/almacena y que existen mientra se este ejecutando la aplicacion
		//archivo de configuracion similar al de hibernate
		ClimaController climaController =(ClimaController) context.getBean("climaController");
		//muestra como spring crea un solo objeto aunque se invoque repetidas veces
		System.out.print("Bean: en main\n" + context.getBean("climaService"));
		System.out.print("\n");
		System.out.print("Bean2: en main\n" + context.getBean("climaService"));
		System.out.print("\n");

		climaController.consultarTemperatura(1);
		climaController.consultarTemperatura(10);
		
		
	}
	
}
