package mx.curso.modulo5.uno.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import mx.curso.modulo5.uno.TestConfig;
import mx.curso.modulo5.uno.TestConfig2;
import mx.curso.modulo5.uno.repository.dto.ClimaDTO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=TestConfig2.class)

public class ClimaDatabaseTest2 {
	
	@Autowired
	private  ClimaDatabase climaDatabase;
	@Autowired
	private  JdbcTemplate mockJdbcTemplate;
	
	
	@SuppressWarnings("unchecked")
//	@Test(expected = RuntimeException.class)
	@Test
	public void testGetPorCiudadId() {
		ClimaDTO mockClimaDTO=new ClimaDTO(1,25);
		
		
		Mockito.when(mockJdbcTemplate.queryForObject(
				Mockito.eq(ClimaDatabase.GET_CLIMA_POR_CIUDAD_ID),
				Mockito.any(RowMapper.class),
				Mockito.eq(1)))
		.thenReturn(mockClimaDTO);
		
		ClimaDTO climaDTO=climaDatabase.getPorCiudadId(1);
		assertEquals(Integer.valueOf(1),climaDTO.getCiudadId());
		assertEquals(Integer.valueOf(25),climaDTO.getTemperatura());
	}
	
	@SuppressWarnings("unchecked")	
	@Test
	public void testGetPorCiudadIdNull() {	
		
		Mockito.when(mockJdbcTemplate.queryForObject(
				Mockito.eq(ClimaDatabase.GET_CLIMA_POR_CIUDAD_ID),
				Mockito.any(RowMapper.class),
				Mockito.eq(1)))
		.thenThrow(new RuntimeException());
		
		ClimaDTO climaDTO=climaDatabase.getPorCiudadId(1);
		assertNull(climaDTO);
	
	}
	
	@Test
	public void testGetTotalClimas() {
		Mockito.when(mockJdbcTemplate.queryForObject(ClimaDatabase.GET_TOTAL_CLIMAS,Integer.class)).thenReturn(200);
		Integer totalClimas=climaDatabase.getTotalClimas();
		assertEquals(Integer.valueOf(200),totalClimas);
	}
	
	
	@After
	public void tearDown(){
		Mockito.clearInvocations(mockJdbcTemplate);
		
	}

	

}
