package mx.curso.modulo5.uno.repository;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import mx.curso.modulo5.uno.repository.dto.ClimaDTO;
import mx.curso.modulo5.uno.service.ClimaService;
import mx.curso.modulo5.uno.service.dto.Clima;

public class ClimaDatabaseTest {
	
	private static ClimaDatabase climaDatabase;
	private static JdbcTemplate mockJdbcTemplate;
	

	@BeforeClass
	public static void setUpBeforeClass() throws Exception{
		mockJdbcTemplate=Mockito.mock(JdbcTemplate.class);		
		climaDatabase=new ClimaDatabase(mockJdbcTemplate);		

	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void testGetPorCiudadId() {
		ClimaDTO mockClimaDTO=new ClimaDTO(1,25);
		
		
		Mockito.when(mockJdbcTemplate.queryForObject(
				Mockito.eq(ClimaDatabase.GET_CLIMA_POR_CIUDAD_ID),
				Mockito.any(RowMapper.class),
				Mockito.eq(1)))
		.thenReturn(mockClimaDTO);
		
		ClimaDTO climaDTO=climaDatabase.getPorCiudadId(1);
		assertEquals(Integer.valueOf(1),climaDTO.getCiudadId());
		assertEquals(Integer.valueOf(25),climaDTO.getTemperatura());
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void testGetPorCiudadIdNull() {	
		
		Mockito.when(mockJdbcTemplate.queryForObject(
				Mockito.eq(ClimaDatabase.GET_CLIMA_POR_CIUDAD_ID),
				Mockito.any(RowMapper.class),
				Mockito.eq(1)))
		.thenThrow(new RuntimeException());
		
		ClimaDTO climaDTO=climaDatabase.getPorCiudadId(1);
		assertNull(climaDTO);
	
	}
	
	@Test
	public void testGetTotalClimas() {
		Mockito.when(mockJdbcTemplate.queryForObject(ClimaDatabase.GET_TOTAL_CLIMAS,Integer.class)).thenReturn(200);
		Integer totalClimas=climaDatabase.getTotalClimas();
		assertEquals(Integer.valueOf(200),totalClimas);
	}
	
	
	@After
	public void tearDown(){
		Mockito.clearInvocations(mockJdbcTemplate);
		
	}



	

}
