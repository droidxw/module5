package mx.curso.modulo5.uno;

import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import mx.curso.modulo5.uno.repository.ClimaRepository;

@Configuration
@ComponentScan(basePackages= {
		"mx.curso.modulo5.uno.service",
})

@PropertySource("classpath:application.properties")
public class TestConfig {	

	@Bean
	@Qualifier("climaArchivo")
	public ClimaRepository mockClimaRepository() {
		
		return Mockito.mock(ClimaRepository.class);
	}	

}

