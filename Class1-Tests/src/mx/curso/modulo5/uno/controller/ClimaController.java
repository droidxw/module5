package mx.curso.modulo5.uno.controller;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;

import mx.curso.modulo5.uno.service.ClimaService;
import mx.curso.modulo5.uno.service.dto.Clima;

@Controller
public class ClimaController {
	
	private static Log log = LogFactory.getLog(ClimaController.class);
	
	private ClimaService climaService;
	
	public ClimaController(ClimaService climaService) {
		this.climaService = climaService;
	}
	
	public void consultarTemperatura(Integer ciudadId) {
		Clima clima = climaService.getClimaPorCiudadId(ciudadId);
		if(clima == null) {
			log.info(String.format("El clima para la ciudad %d no existe", ciudadId));
		} else {
			log.info(String.format("La temperatura para la ciudad %d es %d", ciudadId, clima.getTemperatura()));
		}
	}
	
	public void consultarTotalClimas() {
		Integer totalClimas = climaService.getTotalClimas();
		log.info("Total climas: " + totalClimas);
	}
	
	@PostConstruct
	public void inicializar() {
		log.info("Inicializando ClimaController");
	}
	
	@PreDestroy
	public void destruir() {
		log.info("Destruyendo ClimaController");
	}

}