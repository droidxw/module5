package mx.curso.modulo5.uno.service.dto;

public class Clima {
	
	public Integer ciudadId;
	public Integer temperatura;
	
	public Clima(Integer ciudadId, Integer temperatura) {
		this.ciudadId = ciudadId;
		this.temperatura = temperatura;
	}
	
	public Integer getCiudadId() {
		return ciudadId;
	}
	public void setCiudadId(Integer ciudadId) {
		this.ciudadId = ciudadId;
	}
	public Integer getTemperatura() {
		return temperatura;
	}
	public void setTemperatura(Integer temperatura) {
		this.temperatura = temperatura;
	}

}
