package mx.curso.modulo5.uno.service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import mx.curso.modulo5.uno.repository.ClimaRepository;
import mx.curso.modulo5.uno.repository.dto.ClimaDTO;
import mx.curso.modulo5.uno.service.dto.Clima;

@Service
public class ClimaService {
	
	private static Log log = LogFactory.getLog(ClimaService.class);
	
	private ClimaRepository climaRepository;
	
	public ClimaService(@Qualifier("climaArchivo") ClimaRepository climaRepository) {
//	public ClimaService(@Qualifier("climaDatabase") ClimaRepository climaRepository) {
		this.climaRepository = climaRepository;
	}
	
	public Clima getClimaPorCiudadId(Integer ciudadId) {
		ClimaDTO dto = climaRepository.getPorCiudadId(ciudadId);
		if(dto != null) {
			return new Clima(dto.getCiudadId(), dto.getTemperatura());
		}
		return null;
	}
	
	public Integer getTotalClimas() {
		return climaRepository.getTotalClimas();
	}
	
	@PostConstruct
	public void inicializar() {
		log.info("Inicializando ClimaService");
	}
	
	@PreDestroy
	public void destruir() {
		log.info("Destruyendo ClimaService");
	}
}
