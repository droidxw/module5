package mx.curso.modulo5.uno;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

@Configuration
@ComponentScan(basePackages = {
		"mx.curso.modulo5.uno.controller",
		"mx.curso.modulo5.uno.service",
		"mx.curso.modulo5.uno.repository"
})
@PropertySource("classpath:application.properties")
public class ApplicationConfig {
		
	@Bean
	public Resource recurso(@Value("${archivo.ruta1}") Resource recurso) {
		return recurso;
	}
	
	@Bean
    public DataSource dataSource(
    		@Value("${db.usr}") String user,
    		@Value("${db.pwd}") String pwd,
    		@Value("${db.url}") String jdbcUrl,
    		@Value("${db.driver}") String jdbcDriver) {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(jdbcDriver);
        dataSource.setUrl(jdbcUrl);
        dataSource.setUsername(user);
        dataSource.setPassword(pwd);
        return dataSource;
    }
	
	@Bean
	public JdbcTemplate jdbcTemplate(DataSource dataSource) {
		return new JdbcTemplate(dataSource);
	}
	
}
