package mx.curso.modulo5.uno;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import mx.curso.modulo5.uno.controller.ClimaController;

public class Main {
	
	private static Log log = LogFactory.getLog(Main.class);
	
	public static void main(String args[]) {
		log.info("Iniciando aplicacion");
		
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
		context.register(ApplicationConfig.class);
		context.refresh();
		log.info("Contexto cargado");
		
		ClimaController climaController = (ClimaController) context.getBean("climaController");	
		climaController.consultarTemperatura(1);
		climaController.consultarTemperatura(10);
		climaController.consultarTotalClimas();

		context.close();
		log.info("Contexto cerrado");
	}

}
