package mx.curso.modulo5.uno.repository;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import mx.curso.modulo5.uno.repository.dto.ClimaDTO;

@Repository
public class ClimaDatabase implements ClimaRepository {

	private static Log log = LogFactory.getLog(ClimaDatabase.class);
	//se comentan para propositos del test de mockito
//	private static String GET_CLIMA_POR_CIUDAD_ID = "select * from clima where ciudadId = ?";
//	private static String GET_TOTAL_CLIMAS = "select count(*) from clima";
	protected static String GET_CLIMA_POR_CIUDAD_ID = "select * from clima where ciudadId = ?";
	protected static String GET_TOTAL_CLIMAS = "select count(*) from clima";
	
	private JdbcTemplate jdbcTemplate;
	
	public ClimaDatabase(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
//rowmapper con lambda
	public ClimaDTO getPorCiudadId(Integer ciudadId) {
		try {
			return jdbcTemplate.queryForObject(GET_CLIMA_POR_CIUDAD_ID,
					(rs, rowNum) -> new ClimaDTO(rs.getInt("ciudadId"), rs.getInt("temperatura")), ciudadId);
		} catch (Exception e) {
			log.error(e);
			return null;
		}
	}
	
	public Integer getTotalClimas() {
		return jdbcTemplate.queryForObject(GET_TOTAL_CLIMAS, Integer.class);
	}
	
	@PostConstruct
	public void inicializar() {
		log.info("Inicializando ClimaDatabase");
	}
	
	@PreDestroy
	public void destruir() {
		log.info("Destruyendo ClimaDatabase");
	}

}
