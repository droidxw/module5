package mx.curso.modulo5.uno.repository;

import java.io.IOException;
import java.util.Scanner;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Repository;

import mx.curso.modulo5.uno.repository.dto.ClimaDTO;

@Repository
public class ClimaArchivo implements ClimaRepository {
	
	private static Log log = LogFactory.getLog(ClimaArchivo.class);
	
	private Resource recurso;
	
	public ClimaArchivo(Resource recurso) {
		this.recurso = recurso;
	}
	
	public ClimaDTO getPorCiudadId(Integer ciudadId) {
		try(Scanner scanner = new Scanner(recurso.getFile())) {
			while(scanner.hasNext()) {
				String linea = scanner.nextLine();
				String[] tokens = linea.split(",");
				if(tokens[0].equals(ciudadId.toString())) {
					return new ClimaDTO(tokens[0], tokens[1]);
				}
			}
		} catch (IOException e) {
			log.error(e);
		}
		return null;
	}
	
	public Integer getTotalClimas() {
		int totalLineas = 0;
		try(Scanner scanner = new Scanner(recurso.getFile())) {
			while(scanner.hasNextLine()) {
				scanner.nextLine();
				totalLineas++;
			}
		} catch (IOException e) {
			log.error(e);
		}
		return totalLineas;
	}
	
	@PostConstruct
	public void inicializar() {
		log.info("Inicializando ClimaArchivo");
		
	}
	
	@PreDestroy
	public void destruir() {
		log.info("Destruyendo ClimaArchivo");
	}
	
}
