package mx.curso.modulo5.uno.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Repository;

import mx.curso.modulo5.uno.repository.dto.ClimaDTO;

@Repository
public class ClimaLista implements ClimaRepository, InitializingBean, DisposableBean {
	
	private static Log log = LogFactory.getLog(ClimaLista.class);
	
	private List<ClimaDTO> listaClimas;
		
	public ClimaDTO getPorCiudadId(Integer ciudadId) {
		for(ClimaDTO clima : listaClimas) {
			if(clima.getCiudadId().equals(ciudadId)) {
				return clima;
			}
		}
		return null;
	}
	
	public Integer getTotalClimas() {
		return listaClimas.size();
	}

	public void afterPropertiesSet() throws Exception {
		log.info("Inicializando ClimaLista");
		listaClimas = new ArrayList<>();
		listaClimas.add(new ClimaDTO(1, 25));
		listaClimas.add(new ClimaDTO(2, 17));
		listaClimas.add(new ClimaDTO(3, 33));
	}

	public void destroy() throws Exception {
		log.info("Destruyendo ClimaLista");
		listaClimas.clear();
	}
	
}
