package mx.curso.modulo5.uno.repository;

import mx.curso.modulo5.uno.repository.dto.ClimaDTO;

public interface ClimaRepository {
	public ClimaDTO getPorCiudadId(Integer ciudadId);

}
