package mx.curso.modulo5.uno;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import mx.curso.modulo5.uno.controller.ClimaController;
import mx.curso.modulo5.uno.repository.ClimaArchivo;
import mx.curso.modulo5.uno.repository.ClimaRepository;
import mx.curso.modulo5.uno.repository.ClimaRepositoryImpl;
import mx.curso.modulo5.uno.service.ClimaService;

@Configuration
@ComponentScan(basePackages= {
		"mx.curso.modulo5.uno.controller",
		"mx.curso.modulo5.uno.repository"		
})

public class ApplicationConfig {
	
//	@Bean
//	public ClimaController climaController(ClimaService climaService){
//		
//		return new ClimaController(climaService);
//	}
	
	@Bean
	public ClimaService climaService(@Qualifier("climaRepositoryImpl") ClimaRepository climaRepositoryImpl, 
			@Qualifier("climaArchivo")ClimaRepository climaRepository){
		
		return new  ClimaService(  climaRepository, climaRepositoryImpl);
	}
	
	
	@Bean(destroyMethod="destruir", initMethod="inicializar")
	public ClimaRepository climaArchivo(){
		
		return new  ClimaArchivo();
	}
	
//	@Bean
//	public ClimaRepository climaRepository(){
//		
//		return new ClimaRepositoryImpl();
//	}
//	
//	@Bean
//	public ClimaRepository climaArchivo(){
//		
//		return new ClimaArchivo();
//	}
//	
	
	
	

}
