package mx.curso.modulo5.uno.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

//import mx.curso.modulo5.uno.repository.ClimaRepository;
import mx.curso.modulo5.uno.repository.ClimaArchivo;
import mx.curso.modulo5.uno.repository.ClimaRepository;
import mx.curso.modulo5.uno.repository.dto.ClimaDTO;
import mx.curso.modulo5.uno.service.dto.Clima;
//@Component
//@Service("climaService2")
@Service
public class ClimaService {
//	@Autowired
	
	//al haber una interfaz y clases que implementen se requiere 
	//para distinguir que clase se utilizara 
//	@Qualifier("climaArchivo")marca error al no existir despues de remover la configuracion manual en xml
//	@Qualifier("climaRepositoryImpl")
	private ClimaRepository climaRepository;
//	private ClimaArchivo climaRepository;
	private ClimaRepository climaRepositoryImpl;
//public ClimaService(ClimaRepository climaRepository) {
//		
////		this.climaRepository = new ClimaRepository();
////		this.climaArchivo = new ClimaArchivo(); estaba mal
//	this.climaRepository = climaRepository;
//	}
	
	
	public ClimaService(@Qualifier("climaRepositoryImpl") ClimaRepository climaRepository, 
			ClimaRepository climaRepositoryImpl) {
this.climaRepository = climaRepository;
this.climaRepositoryImpl = climaRepositoryImpl;
}

public Clima getClimaPorCiudadId(Integer ciudadId) {
	
//	ClimaRepository climaRepository=new ClimaRepository();
	
//	ClimaArchivo climaArchivo=new ClimaArchivo();
//	ClimaDTO dto =climaArchivo.getPorCiudadId(ciudadId);
	ClimaDTO dto =climaRepository.getPorCiudadId(ciudadId);
	if(dto!=null) {
		return new Clima(dto.getCiudadId(), dto.getTemperatura());
			}
	
	return null;
	
		
	}

//		public void setClimaRepository(ClimaRepository climarepository) {
//			this.climarepository=climarepository;
//		}
}
