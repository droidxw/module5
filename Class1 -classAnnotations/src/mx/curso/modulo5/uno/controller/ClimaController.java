package mx.curso.modulo5.uno.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;

import mx.curso.modulo5.uno.repository.ClimaRepository;
import mx.curso.modulo5.uno.service.ClimaService;
import mx.curso.modulo5.uno.service.dto.Clima;
//@Component
@Controller
public class ClimaController {
	
//	@Autowired
	
	//no tiene mucho sentido, aunque no causara error
	//ya que aqui faltaría una interfaz y clases que implementen
//	@Qualifier("climaService2")
	private ClimaService climaService;
	
//	public ClimaController(ClimaService climaService) {
////		this.climaservice=new ClimaService();
//		this.climaService=climaService;
//	}
	
	
public ClimaController(ClimaService climaService) {

this.climaService=climaService;
}

	
	public void consultarTemperatura(Integer ciudadId) {
//		ClimaService climaService=new ClimaService();
		Clima clima=climaService.getClimaPorCiudadId(ciudadId);
		if(clima==null) {
			System.out.printf("El clima para la ciudad %d no existe \n", ciudadId);
		}else {
			
			System.out.printf("La temperatura para la ciudad %d es %d \n", clima.getCiudadId(), clima.getTemperatura());
		}	
		
	}
	
//	public void setClimaService(ClimaService climaservice) {
//		this.climaservice=climaservice;
//	}
}
