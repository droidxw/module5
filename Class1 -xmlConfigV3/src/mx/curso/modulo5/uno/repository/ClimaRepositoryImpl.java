package mx.curso.modulo5.uno.repository;

import java.util.ArrayList;
import java.util.List;


import mx.curso.modulo5.uno.repository.dto.ClimaDTO;



public class ClimaRepositoryImpl implements ClimaRepository {

	private List<ClimaDTO> listaClimas;
	
	
	public ClimaRepositoryImpl() {
		listaClimas=new ArrayList<>();
		listaClimas.add(new ClimaDTO(1,25));
		listaClimas.add(new ClimaDTO(2,17));
		listaClimas.add(new ClimaDTO(3,33));
				
	}
	
	public ClimaDTO getPorCiudadId(Integer ciudadId) {
		for(ClimaDTO clima: listaClimas) {
			
				if(clima.getCiudadId().equals(ciudadId)) {
					return clima;
					
				}
			}
			return null;
		}
		
		
	}
	
	

