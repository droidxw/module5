package mx.curso.modulo5.uno.service;

import mx.curso.modulo5.uno.repository.ClimaRepository;
import mx.curso.modulo5.uno.repository.dto.ClimaDTO;
import mx.curso.modulo5.uno.service.dto.Clima;

public class ClimaService {

	private ClimaRepository climaRepository;

public ClimaService() {		
	//esta instanciacion se remplaza con spring en el xml
//		this.climaRepository = new ClimaRepository();
	}

public Clima getClimaPorCiudadId(Integer ciudadId) {
	

	ClimaDTO dto =climaRepository.getPorCiudadId(ciudadId);
	if(dto!=null) {
		return new Clima(dto.getCiudadId(), dto.getTemperatura());
			}
	
	return null;
	
		
	}
// se crea un setter para que spring pueda inyectar el bean
		public void setClimaRepository(ClimaRepository climaRepository) {
			this.climaRepository=climaRepository;
		}
}
