package mx.curso.modulo5.uno.repository;

import java.io.File;

import java.io.IOException;
import java.util.Scanner;
import org.springframework.stereotype.Repository;

import mx.curso.modulo5.uno.repository.dto.ClimaDTO;

//si no se definen no pasa nada, se utilizan para casuisticas especiales
@Repository
public class ClimaArchivo implements ClimaRepository  {
	//se puede usar en conjunto con la Forma 2 (notaciones)o Forma 3 (Interfaz)
//Forma 1: forma manual de inicializacion de beans
	
	//Esto es con una clase de configuracion de Java (ApplicatopnConfig en este caso) 
	//definicion del constructor y asignacion de dependencias y propiedades 
	//para posterior inyeccion
	public ClimaArchivo() {

	}	
	
	public ClimaDTO getPorCiudadId(Integer ciudadId) {
		try {
			File archivo=new File(getArchivo());		
			Scanner sc=new Scanner (archivo);
			while(sc.hasNextLine()) {
				String linea=sc.nextLine();
				String []valores=linea.split(",");
				if(valores[0].equals(ciudadId.toString())) {
					return new ClimaDTO(valores[0],valores[1]);
				}	
			}

		}catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	
	
	private String getArchivo() {
		// TODO Auto-generated method stub
		return getClass().getClassLoader().getResource("clima.csv").getFile();
	}

	//se inicializan los beans despues de la inyeccion 
	private void inicializar() {		
		System.out.println("Inicializando ClimaArchivo");		
	}
	
	//al terminar de usar la aplicacion (cierre del contexto de sprint) se llama este metodo
	//esto antes de que se destruya el bean de inicio
private void destruir() {		
		System.out.println("Destruyendo ClimaArchivo");		
	}
		

}
