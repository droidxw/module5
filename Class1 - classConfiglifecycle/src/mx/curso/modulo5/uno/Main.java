package mx.curso.modulo5.uno;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;


import mx.curso.modulo5.uno.controller.ClimaController;

public class Main {
	private static Log log=LogFactory.getLog(ClimaController.class);

	public static void main(String args[]) {		
		AnnotationConfigApplicationContext context=new  AnnotationConfigApplicationContext();
		context.register(ApplicationConfig.class);
		context.refresh();

		ClimaController climaController =(ClimaController) context.getBean("climaController");
		log.info("testClimaControllerMain//"+climaController);
		climaController.consultarTemperatura(1);
		climaController.consultarTemperatura(10);
		//se debe cerrar el contexto con esta instruccion 
//		para que se puedan invocar los metodos de destruccion de beans definidos 
		context.close();

	}
		
		
		
		
	
	
	
	
}
