package mx.curso.modulo5.uno.service.dto;
//POJOS

//atributos privados y constructores 
//Getters y Setters 
public class Clima {
	
	private Integer ciudadId;
	public Integer getCiudadId() {
		return ciudadId;
	}

	public void setCiudadId(Integer ciudadId) {
		this.ciudadId = ciudadId;
	}

	public Integer getTemperatura() {
		return temperatura;
	}

	public void setTemperatura(Integer temperatura) {
		this.temperatura = temperatura;
	}

	private Integer temperatura;
	
	public Clima(Integer ciudadId, Integer temperatura) {
		this.ciudadId=ciudadId;
		this.temperatura=temperatura;
	}

}
