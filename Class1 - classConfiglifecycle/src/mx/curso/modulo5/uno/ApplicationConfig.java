package mx.curso.modulo5.uno;
//Mezcla entre anotaciones tipo component y definicion de beans
//en clase de configuracion (en vez de configuracion en XML)
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


import mx.curso.modulo5.uno.repository.ClimaArchivo;
import mx.curso.modulo5.uno.repository.ClimaRepository;
import mx.curso.modulo5.uno.repository.ClimaRepositoryImpl;

@Configuration
@ComponentScan(basePackages= {
		"mx.curso.modulo5.uno.controller",
		"mx.curso.modulo5.uno.repository",
		"mx.curso.modulo5.uno.service"
})

public class ApplicationConfig{
	
	//En spring por defecto crea el bean mientras 
//	la aplicacion siga activa en el scope singleton	
	@Bean
	public ClimaRepository climaRepository(){
		
		return new ClimaRepositoryImpl();
	}

	//método de destruccion e inicializacion de bean
	@Bean(destroyMethod="destruir", initMethod="inicializar")	
	public ClimaRepository climaArchivo() {
		///en el llamado de esta clase estan los dos métodos defuinidos en el bean 
		return new  ClimaArchivo();
	}
		

}
