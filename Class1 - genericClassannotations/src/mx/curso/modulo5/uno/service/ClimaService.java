package mx.curso.modulo5.uno.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

//import mx.curso.modulo5.uno.repository.ClimaRepository;
import mx.curso.modulo5.uno.repository.ClimaArchivo;
import mx.curso.modulo5.uno.repository.ClimaRepository;
import mx.curso.modulo5.uno.repository.dto.ClimaDTO;
import mx.curso.modulo5.uno.service.dto.Clima;

@Component
public class ClimaService {
	@Autowired
	//al haber una interfaz y clases que implementen se requiere 
	//para distinguir que clase se utilizara 
//	@Qualifier("climaArchivo")marca error al no existir despues de remover la configuracion manual en xml
	@Qualifier("climaRepositoryImpl")
	private ClimaRepository climaRepository;

public Clima getClimaPorCiudadId(Integer ciudadId) {

	ClimaDTO dto =climaRepository.getPorCiudadId(ciudadId);
	if(dto!=null) {
		return new Clima(dto.getCiudadId(), dto.getTemperatura());
			}
	
	return null;
	
		
	}

}
