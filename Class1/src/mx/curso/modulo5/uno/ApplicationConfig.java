package mx.curso.modulo5.uno;

import java.io.File;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.core.io.Resource;

import mx.curso.modulo5.uno.controller.ClimaController;
import mx.curso.modulo5.uno.repository.ClimaArchivo;
import mx.curso.modulo5.uno.repository.ClimaRepository;
import mx.curso.modulo5.uno.repository.ClimaRepositoryImpl;
import mx.curso.modulo5.uno.service.ClimaService;

@Configuration
@ComponentScan(basePackages= {
		"mx.curso.modulo5.uno.controller",
		"mx.curso.modulo5.uno.repository",
		"mx.curso.modulo5.uno.service"
})



@PropertySource("classpath:application.properties")
public class ApplicationConfig {
	
//	@Bean
//	public ClimaController climaController(ClimaService climaService){
//		
//		return new ClimaController(climaService);
//	}
	
//	@Bean
//	@Scope("prototype")
//	public ClimaService climaService(@Qualifier("climaRepositoryImpl") ClimaRepository climaRepositoryImpl, 
//			@Qualifier("climaArchivo")ClimaRepository climaRepository){
//		
//		return new  ClimaService(  climaRepository, climaRepositoryImpl);
//	}
//	
//	
//	@Bean(destroyMethod="destruir", initMethod="inicializar")
//	@Scope("prototype")
//	public ClimaRepository climaArchivo(){
//		
//		return new  ClimaArchivo();
//	}
	@Bean(destroyMethod="destruir", initMethod="inicializar")
	@Scope("prototype")
	public ClimaRepository climaArchivo(@Value("${archivo.nombre}") String nombreArchivo, @Value("file:C:\\workspace\\Class1\\resources\\clima.csv") Resource recurso) {
//		"file:C:\\Users\\clima.csv"
		return new  ClimaArchivo(nombreArchivo, recurso);
	}
	
//	@Bean
//	public ClimaRepository climaRepository(){
//		
//		return new ClimaRepositoryImpl();
//	}
//	
//	@Bean
//	public ClimaRepository climaArchivo(){
//		
//		return new ClimaArchivo();
//	}
//	
	
	
	

}
