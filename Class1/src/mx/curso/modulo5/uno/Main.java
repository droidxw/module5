package mx.curso.modulo5.uno;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.GenericApplicationContext;

import mx.curso.modulo5.uno.controller.ClimaController;

public class Main {
	private static Log log=LogFactory.getLog(ClimaController.class);

	public static void main(String args[]) {
		
//		GenericApplicationContext context=new  GenericApplicationContext();
//		new XmlBeanDefinitionReader(context).loadBeanDefinitions("application-context.xml");
//		context.refresh();
		
		AnnotationConfigApplicationContext context=new  AnnotationConfigApplicationContext();
		context.register(ApplicationConfig.class);
		context.refresh();
		
		
		
//		ClimaController climaController =new ClimaController();
		ClimaController climaController =(ClimaController) context.getBean("climaController");
		log.info("testClimaControllerMain//"+climaController);
		climaController.consultarTemperatura(1);
		climaController.consultarTemperatura(10);
		
		
		ClimaController climaController2 =(ClimaController) context.getBean("climaController");
		log.info("testClimaControllerMain2//"+climaController2);
		climaController.consultarTemperatura(1);
		climaController.consultarTemperatura(10);
		context.close();
		
	}
		
		
		
		
	
	
	
	
}
