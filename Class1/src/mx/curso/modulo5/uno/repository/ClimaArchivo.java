package mx.curso.modulo5.uno.repository;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import mx.curso.modulo5.uno.repository.dto.ClimaDTO;
//@Component

@Repository
public class ClimaArchivo implements ClimaRepository  {
	
	private String nombreArchivo;
	private Resource recurso;
	
	public ClimaArchivo(String nombreArchivo, Resource recurso) {
		this.nombreArchivo= nombreArchivo;
		this.recurso=recurso;
	}
	
	
	public ClimaDTO getPorCiudadId(Integer ciudadId) {
		try {
//			File archivo=new File(getArchivo());
			File archivo= recurso.getFile();
			Scanner sc=new Scanner (archivo);
			while(sc.hasNextLine()) {
				String linea=sc.nextLine();
				String []valores=linea.split(",");
				if(valores[0].equals(ciudadId.toString())) {
					return new ClimaDTO(valores[0],valores[1]);
				}	
			}

		}catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	
	
	private String getArchivo() {
		// TODO Auto-generated method stub
		return getClass().getClassLoader().getResource(nombreArchivo).getFile();
	}



	private List<ClimaDTO> listaClimas;
	
	
	public ClimaArchivo() {
		listaClimas=new ArrayList<>();
		listaClimas.add(new ClimaDTO(1,12));
		listaClimas.add(new ClimaDTO(2,24));
		listaClimas.add(new ClimaDTO(3,16));
				
	}
	
//	public ClimaDTO getPorCiudadId(Integer ciudadId) {
//		for(ClimaDTO clima: listaClimas) {
//			
//				if(clima.getCiudadId().equals(ciudadId)) {
//					return clima;
//					
//				}
//			}
//			return null;
//		}
	
//	public ClimaDTO getPorCiudadId(Integer ciudadId) {
//		for(ClimaDTO clima: listaClimas) {
//			
//				if(clima.getCiudadId().equals(ciudadId)) {
//					return clima;
//					
//				}
//			}
//			return null;
//		}
	
	
	private void inicializar() {
		
		System.out.println("Inicializando ClimaArchivo");
		
	}
	
	
private void destruir() {
		
		System.out.println("Destruyendo ClimaArchivo");
		
	}
		

}
