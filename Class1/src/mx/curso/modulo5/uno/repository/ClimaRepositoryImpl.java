package mx.curso.modulo5.uno.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import mx.curso.modulo5.uno.controller.ClimaController;
import mx.curso.modulo5.uno.repository.dto.ClimaDTO;

//import com.sun.tools.javac.util.List;
//@Component//coneste spring sabe que esta clase es un bean

@Repository
public class ClimaRepositoryImpl implements ClimaRepository,
InitializingBean, DisposableBean {//climaRepositoryImpl
	private static Log log=LogFactory.getLog(ClimaController.class);

	private List<ClimaDTO> listaClimas;

	
	public ClimaRepositoryImpl() {
		
//		listaClimas=new ArrayList<>();
//		listaClimas.add(new ClimaDTO(1,25));
//		listaClimas.add(new ClimaDTO(2,17));
//		listaClimas.add(new ClimaDTO(3,33));
				
	}
	
	public ClimaDTO getPorCiudadId(Integer ciudadId) {
		for(ClimaDTO clima: listaClimas) {
			
				if(clima.getCiudadId().equals(ciudadId)) {
					return clima;
					
				}
			}
			return null;
		}


	@Override
	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub
		listaClimas=new ArrayList<>();
		listaClimas.add(new ClimaDTO(1,25));
		listaClimas.add(new ClimaDTO(2,17));
		listaClimas.add(new ClimaDTO(3,33));
		log.info("climaRepo"+listaClimas);
	}
	
	

	@Override
	public void destroy() throws Exception {
		// TODO Auto-generated method stub
		listaClimas.clear();
	}
		
		
	}
	
	

