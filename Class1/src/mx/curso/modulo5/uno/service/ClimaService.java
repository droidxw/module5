package mx.curso.modulo5.uno.service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import mx.curso.modulo5.uno.controller.ClimaController;
//import mx.curso.modulo5.uno.repository.ClimaRepository;
import mx.curso.modulo5.uno.repository.ClimaArchivo;
import mx.curso.modulo5.uno.repository.ClimaRepository;
import mx.curso.modulo5.uno.repository.dto.ClimaDTO;
import mx.curso.modulo5.uno.service.dto.Clima;
//@Component
//@Service("climaService2")

@Service
@Scope("prototype")
public class ClimaService {
	private static Log log=LogFactory.getLog(ClimaController.class);
//	@Autowired
	
	//al haber una interfaz y clases que implementen se requiere 
	//para distinguir que clase se utilizara 
//	@Qualifier("climaArchivo")marca error al no existir despues de remover la configuracion manual en xml
	
	
	private ClimaRepository climaRepository;
	
	@Value("${db.user}")
	private String username;
	@Value("${db.pwd}")
	private String password;
	
	
//	private ClimaArchivo climaRepository;
	private ClimaRepository climaRepositoryImpl;
//public ClimaService(ClimaRepository climaRepository) {
//		
////		this.climaRepository = new ClimaRepository();
////		this.climaArchivo = new ClimaArchivo(); estaba mal
//	this.climaRepository = climaRepository;
//	}	
	

//	@Autowired
	public ClimaService(@Qualifier("climaArchivo") ClimaRepository climaRepository, 
			
			ClimaRepository climaRepositoryImpl) {
			log.info("testClimaService//"+climaRepository);	
		
			this.climaRepository = climaRepository;
			this.climaRepositoryImpl = climaRepositoryImpl;			
			
}
	
	
//	@Autowired
//	public ClimaService(@Qualifier("climaArchivo") ClimaRepository climaRepository) {
//			log.info("testClimaService//"+climaRepository);	
//		
//			this.climaRepository = climaRepository;
//					
//			
//}
//	
	
	

	
	

public Clima getClimaPorCiudadId(Integer ciudadId) {
	
//	ClimaRepository climaRepository=new ClimaRepository();
	
//	ClimaArchivo climaArchivo=new ClimaArchivo();
//	ClimaDTO dto =climaArchivo.getPorCiudadId(ciudadId);
	ClimaDTO dto =climaRepository.getPorCiudadId(ciudadId);
	if(dto!=null) {
		return new Clima(dto.getCiudadId(), dto.getTemperatura());
			}
	
	return null;
	
		
	}

@PostConstruct
public void inicializar() {
	System.out.println("Inicializando ClienteService");
	log.info("testClimaControllerMain2//");
	log.info("testClimaControllerMain2//");
	log.info("username:"+username);
	log.info("password:"+password);
}



@PreDestroy
public void destruir() {
	System.out.println("Destruyendo ClienteService");
	
}


//		public void setClimaRepository(ClimaRepository climarepository) {
//			this.climarepository=climarepository;
//		}
}
